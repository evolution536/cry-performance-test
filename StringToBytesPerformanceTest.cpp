#include "CryTestFunctions.h"

// ---------------------------------------------------------------------------------------------

// This is the original function and runs pretty slow.
ArrayOfBytes StringToBytesOriginal(const String& input)
{
	int dataIndex = 0;
	ArrayOfBytes aob;
	aob.Data = new Byte[input.GetLength()];
	String tmpLoopStr;
	
	for (int c = 0; c <= input.GetLength(); c++)
	{
		if (input[c] == 0x20 || input[c] == 0x0) // scan for space/null character in string
		{
			aob.Data[dataIndex++] = (Byte)ScanInt(tmpLoopStr, NULL, 16);
			
			tmpLoopStr.Clear();
		}
		else
		{
			tmpLoopStr += input[c];
		}
	}
	
	aob.Size = dataIndex;
	
	return aob;
}

// This is the optimized version using string iteration. Runs around 15% faster than the original function.
ArrayOfBytes StringToBytesOptimized(const String& input)
{
	int dataIndex = 0;
	const int inputLength = input.GetLength();
	Byte* const dataPtr = new Byte[inputLength];
	char* const tmpLoopStr = new char[inputLength];
	int nullCounter = 0;
	const char* iterator = input.Begin();
	const char* const endIterator = iterator + inputLength;
	
	while (iterator <= endIterator)
	{
		if (*iterator == 0x20 || *iterator == 0x0)
		{
			dataPtr[dataIndex++] = (Byte)ScanInt(tmpLoopStr, NULL, 16);
			nullCounter = 0;
		}
		else
		{
			tmpLoopStr[nullCounter++] = *iterator;
			tmpLoopStr[nullCounter] = 0;
		}
		
		++iterator;
	}
	
	delete[] tmpLoopStr;
	
	ArrayOfBytes aob;
	aob.Data = dataPtr;
	aob.Size = dataIndex;
	return aob;
}

// Utility macros that convert a character representation to its value as byte.
#define INRANGE(x,a,b)		(x >= a && x <= b) 
#define getBits(x)			(INRANGE((x&(~0x20)), 'A', 'F') ? ((x & (~0x20)) - 'A' + 0xA) : (INRANGE(x, '0', '9') ? x - '0' : 0))
#define getByte(x)			(getBits(x[0]) << 4 | getBits(x[1]))

// This function contains an even more optimized algorithm using the above macros. (3 times faster than the original).
ArrayOfBytes StringToBytesLearnMoreOptimized(const String& input)
{
	int dataIndex = 0;
	const int inputLength = input.GetLength();
	Byte* const dataPtr = new Byte[inputLength];
	const char* iterator = input.Begin();
	const char* const endIterator = iterator + inputLength;
	
	while (iterator < endIterator)
	{
		if (*iterator == 0x20 || *iterator == 0x0)
		{
			++iterator;
		}
		else
		{
			dataPtr[dataIndex++] = getByte(iterator);
			iterator += 2;
		}
	}
	
	return ArrayOfBytes(dataPtr, dataIndex);
}