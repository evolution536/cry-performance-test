#include "CryTestFunctions.h"

// Original function implementing Vector<bool>.
const int VectorBoolOrBitsetTestVectorBool(Vector<bool>& buffer, const Vector<unsigned char>& rand)
{
	const int count = rand.GetCount();
	for (int i = 0; i < count; ++i)
	{
		buffer.At(i) = rand[i] > 50 ? true : false;
	}
	return buffer.GetAlloc();
}

// Different function implementing Bits.
const int VectorBoolOrBitsetTestBitSet(Bits& buffer, const Vector<unsigned char>& rand)
{
	const int count = rand.GetCount();
	for (int i = 0; i < count; ++i)
	{
		buffer.Set(i, rand[i] > 50 ? true : false);
	}
	int alloc = 0;
	buffer.Raw(alloc);
	return alloc;
}

// Different function implementing std::bitset.
const int VectorBoolOrBitsetTestStdBitSet(std::bitset<100000>& buffer, const Vector<unsigned char>& rand)
{
	int x = 0;
	const int count = rand.GetCount();
	for (int i = 0; i < count; ++i)
	{
		buffer.set(i, rand[i] > 50 ? true : false);
	}
	return buffer.size();
}

// Utility union.
union LowHighDword
{
	dword dw;
	struct
	{
		word w1;
		word w2;
	};
};

// Different function implementing Bits, but with a vectorized set method.
const int VectorBoolOrBitsetTestBitSetVectorized(Bits& buffer, const Vector<unsigned char>& rand)
{
	// The vector length is 32, and we have to assume that the internal buffer of the vector
	// rand is aligned to a 16-byte boundary!
	const int count = rand.GetCount();
	for (int i = 0; i < count; i += 32)
	{
		// Generate a comparison mask.
		__m128i vecToCmp = _mm_set1_epi8((char)50);
		
		// Load the low- and high vector (32 elements) into vector variables.
		__m128i cmpVecLow = _mm_load_si128((__m128i*)&rand[i]);
		__m128i cmpVecHigh = _mm_load_si128((__m128i*)&rand[i + 16]);
		
		// Compare the low- and high vector elements.
		__m128i resMaskLow = _mm_cmpgt_epi8(cmpVecLow, vecToCmp);
		__m128i resMaskHigh = _mm_cmpgt_epi8(cmpVecHigh, vecToCmp);
		
		// Create the bitmask for low and high and store it in a dword to pass blindly to the bitset.
		const int maskLow = _mm_movemask_epi8(resMaskLow);
		const int maskHigh = _mm_movemask_epi8(resMaskHigh);
		
		// Create the final bitset content.
		LowHighDword w;
		w.w1 = (short)maskLow;
		w.w2 = (short)maskHigh;
		
		// Use the vectorized set method.
		buffer.Set(i, w.dw, 32);
	}
	int alloc = 0;
	buffer.Raw(alloc);
	return alloc;
}

// Same vectorized function implementing Bits, but with AVX2 vector instructions.
const int VectorBoolOrBitsetTestBitSetVectorizedAVX2(Bits& buffer, const Vector<unsigned char>& rand)
{
	// The vector length is 32, and we have to assume that the internal buffer of the vector
	// rand is aligned to a 32-byte boundary!
	const int count = rand.GetCount();
	for (int i = 0; i < count; i += 32)
	{
		// Generate a comparison mask.
		__m256i vecToCmp = _mm256_set1_epi8((char)50);
		
		// Load the vector (32 elements) into vector variables.
		__m256i cmpVec = _mm256_load_si256((__m256i*)&rand[i]);
		
		// Compare the vector elements.
		__m256i resMask = _mm256_cmpgt_epi8(cmpVec, vecToCmp);
		
		// Create the bitmask for the bitset to pass blindly to the set function.
		const int mask = _mm256_movemask_epi8(resMask);
		
		// Use the vectorized set method.
		buffer.Set(i, mask, 32);
	}
	int alloc = 0;
	buffer.Raw(alloc);
	return alloc;
}