#include "PerformanceTest.h"
#include "PerformanceTestManager.h"

PerformanceTest::PerformanceTest()
{
	
}

PerformanceTest::~PerformanceTest()
{
	
}

// Starts the timer for the performance test.
void PerformanceTest::Start()
{
	// Get the amount of ticks per second.
	QueryPerformanceFrequency(&this->frequency);

	// Start the timer.
	QueryPerformanceCounter(&this->t1);
}

// Stops the performance test.
void PerformanceTest::Stop()
{
	// Stop the timer.
	QueryPerformanceCounter(&this->t2);
}

// Returns the elapsed time represented as a double value.
const double PerformanceTest::GetElapsedTime() const
{
	return (this->t2.QuadPart - this->t1.QuadPart) * 1000.0 / this->frequency.QuadPart;
}