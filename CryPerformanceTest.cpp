#include "CryPerformanceTest.h"
#include "CryAboutPerformanceTest.h"
#include "PerformanceTest.h"
#include "CryTestFunctions.h"
#include "ImlProvider.h"
#include <bitset>

// ---------------------------------------------------------------------------------------------

// Global source IML file declaration. Imaging in the GUI depends on this.
#define IMAGECLASS CryPerformanceTestIml
#define IMAGEFILE "CryPerformanceTest.iml"
#include <Draw/iml_source.h>

// ---------------------------------------------------------------------------------------------

Vector<TestCase> GlobalDefinedTests;

// ---------------------------------------------------------------------------------------------

CryPerformanceTest::CryPerformanceTest()
{
	this->Title("Cry Performance Analysis").Icon(CryPerformanceTestIml::CryPerformanceTest()).Sizeable().Zoomable().SetRect(0, 0, 550, 300);
	
	this->AddFrame(mMenuStrip);
	this->mMenuStrip.Set(THISBACK(MainMenu));
	
	*this
		<< this->mPerformanceTests.HSizePos(5, 5).VSizePos(5, 5)
	;
	
	this->mPerformanceTests.AddColumn("Function", 45);
	this->mPerformanceTests.AddColumn("Result", 28);
	this->mPerformanceTests.AddColumn("Timing", 27);
	
	// Add the BytesToString testcase.
	TestCase& bytesString = GlobalDefinedTests.Add();
	bytesString.TestName = "BytesToString";
	bytesString.TestFunction = THISBACK(BytesStringRunTest);
	
	// Add the StringToBytes testcase.
	TestCase& stringBytes = GlobalDefinedTests.Add();
	stringBytes.TestName = "StringToBytes";
	stringBytes.TestFunction = THISBACK(StringBytesRunTest);
	
	// Add the Ipv4ToString testcase.
	TestCase& ipv4String = GlobalDefinedTests.Add();
	ipv4String.TestName = "Ipv4ToString";
	ipv4String.TestFunction = THISBACK(Ipv4ToStringRunTest);
	
	// Add the EvoNetflowCollector StringStream testcase.
	TestCase& evoStringStream = GlobalDefinedTests.Add();
	evoStringStream.TestName = "EvoStringStream";
	evoStringStream.TestFunction = THISBACK(EvoStringStreamRunTest);
	
	// Add the AOB comparison testcase.
	TestCase& aobComparison = GlobalDefinedTests.Add();
	aobComparison.TestName = "AOBComparison";
	aobComparison.TestFunction = THISBACK(EvoAOBComparisonTest);
	
	// Add the RunForOne testcase.
	TestCase& runForOne = GlobalDefinedTests.Add();
	runForOne.TestName = "RunForOne";
	runForOne.TestFunction = THISBACK(RunForOneTest);
	
	// Add the GetModuleFromContainedAddress testcase.
	TestCase& vectorBoolVsBits = GlobalDefinedTests.Add();
	vectorBoolVsBits.TestName = "VectorBoolOrBitset";
	vectorBoolVsBits.TestFunction = THISBACK(VectorBoolOrBitsetTest);
}

CryPerformanceTest::~CryPerformanceTest()
{
	
}

void CryPerformanceTest::MainMenu(Bar& pBar)
{
	pBar.Add("File", THISBACK(FileMenu));
	pBar.Add("Help", THISBACK(HelpMenu));
}

void CryPerformanceTest::FileMenu(Bar& pBar)
{
	pBar.Add("Run Tests", THISBACK(RunTestsMenu));
	pBar.Add("Exit", CryPerformanceTestIml::ExitButton(), THISBACK(ExitApplication));
}

void CryPerformanceTest::HelpMenu(Bar& pBar)
{
	pBar.Add("About", CryPerformanceTestIml::AboutButton(), THISBACK(AboutApplication));
}

void CryPerformanceTest::RunTestsMenu(Bar& pBar)
{
	for (int i = 0; i < GlobalDefinedTests.GetCount(); ++i)
	{
		pBar.Add(GlobalDefinedTests[i].TestName, GlobalDefinedTests[i].TestFunction);
	}
}

void CryPerformanceTest::ExitApplication()
{
	this->Close();
}

void CryPerformanceTest::AboutApplication()
{
	CryAboutPerformanceTest* capt = new CryAboutPerformanceTest();
	capt->Execute();
	delete capt;
}

// ---------------------------------------------------------------------------------------------

void CryPerformanceTest::BytesStringRunTest()
{
	PerformanceTest test;
	String result;
	this->mPerformanceTests.Clear();
	
	test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = BytesToStringOriginal(constArray, _countof(constArray));
	}
	test.Stop();
	this->mPerformanceTests.Add("BytesToStringOriginal", result, test.GetElapsedTime());
	
	test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = BytesToStringEvoSolution(constArray, _countof(constArray));
	}
	test.Stop();
	this->mPerformanceTests.Add("BytesToStringEvoSolution", result, test.GetElapsedTime());
	
		test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = BytesToStringCustomizedLearnMore(constArray, _countof(constArray));
	}
	test.Stop();
	this->mPerformanceTests.Add("BytesToStringCustomizedLearnMore", result, test.GetElapsedTime());
	
		test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = BytesToStringCustomizedLearnMoreWithStringBuffer(constArray, _countof(constArray));
	}
	test.Stop();
	this->mPerformanceTests.Add("BytesToStringCustomizedLearnMoreWithStringBuffer", result, test.GetElapsedTime());
	
	test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = BytesToStringSd333221(constArray, _countof(constArray));
	}
	test.Stop();
	this->mPerformanceTests.Add("BytesToStringSd333221", result, test.GetElapsedTime());
	
	test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = BytesToStringCustomizedLearnMoreWithStringBufferSSE(constArray, _countof(constArray));
	}
	test.Stop();
	this->mPerformanceTests.Add("BytesToStringCustomizedLearnMoreWithStringBufferSSE", result, test.GetElapsedTime());
}

void CryPerformanceTest::StringBytesRunTest()
{
	PerformanceTest test;
	ArrayOfBytes result;
	this->mPerformanceTests.Clear();
	
	test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = StringToBytesOriginal(testString);
	}
	test.Stop();
	this->mPerformanceTests.Add("StringToBytesOriginal", BytesToStringCustomizedLearnMoreWithStringBuffer(result.Data, result.Size), test.GetElapsedTime());
	
	test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = StringToBytesOptimized(testString);
	}
	test.Stop();
	this->mPerformanceTests.Add("StringToBytesOptimized", BytesToStringCustomizedLearnMoreWithStringBuffer(result.Data, result.Size), test.GetElapsedTime());
	
	test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = StringToBytesLearnMoreOptimized(testString);
	}
	test.Stop();
	this->mPerformanceTests.Add("StringToBytesLearnMoreOptimized", BytesToStringCustomizedLearnMoreWithStringBuffer(result.Data, result.Size), test.GetElapsedTime());
}

void CryPerformanceTest::Ipv4ToStringRunTest()
{
	PerformanceTest test;
	String result;
	this->mPerformanceTests.Clear();
	
	test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = Ipv4ToStringOriginal(testIpv4);
	}
	test.Stop();
	this->mPerformanceTests.Add("Ipv4ToStringOriginal", result, test.GetElapsedTime());
	
	test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = Ipv4ToStringOptimizedNoStringStream(testIpv4);
	}
	test.Stop();
	this->mPerformanceTests.Add("Ipv4ToStringOptimizedNoStringStream", result, test.GetElapsedTime());

	test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = Ipv4ToStringOptimizedEvolution536(testIpv4);
	}
	test.Stop();
	this->mPerformanceTests.Add("Ipv4ToStringOptimizedEvolution536", result, test.GetElapsedTime());

	test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = Ipv4ToStringOptimizedTablez(testIpv4);
	}
	test.Stop();
	this->mPerformanceTests.Add("Ipv4ToStringOptimizedTablez", result, test.GetElapsedTime());
}

void CryPerformanceTest::EvoStringStreamRunTest()
{
	PerformanceTest test;
	String result;
	this->mPerformanceTests.Clear();
	
	test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = FlowReceivedCallback5Original();
	}
	test.Stop();
	this->mPerformanceTests.Add("FlowReceivedCallback5Original", result, test.GetElapsedTime());
	
	test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = FlowReceivedCallback5NoStringStream();
	}
	test.Stop();
	this->mPerformanceTests.Add("FlowReceivedCallback5NoStringStream", result, test.GetElapsedTime());

	test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = FlowReceivedCallback5ItoaOptimized();
	}
	test.Stop();
	this->mPerformanceTests.Add("FlowReceivedCallback5ItoaOptimized", result, test.GetElapsedTime());
}

void CryPerformanceTest::EvoAOBComparisonTest()
{
	PerformanceTest test;
	bool result;
	ArrayOfBytes input(constArray, _countof(constArray));
	ArrayOfBytes expected(constCompareArray, _countof(constCompareArray));
	this->mPerformanceTests.Clear();
	
	test.Start();
	for (int i = 0; i < 10000000; ++i)
	{
		result = EvoCompareAobOriginal(input, expected);
	}
	test.Stop();
	this->mPerformanceTests.Add("EvoCompareAobOriginal", result ? "true" : "false", test.GetElapsedTime());
	
	test.Start();
	for (int i = 0; i < 10000000; ++i)
	{
		result = EvoCompareAobOptimized(input, expected);
	}
	test.Stop();
	this->mPerformanceTests.Add("EvoCompareAobOptimized", result ? "true" : "false", test.GetElapsedTime());
	
	input.Data = NULL;
	expected.Data = NULL;
}

void CryPerformanceTest::RunForOneTest()
{
	PerformanceTest test;
	const bool* result;
	this->mPerformanceTests.Clear();
	
	test.Start();
	for (int i = 0; i < 10000000; ++i)
	{
		result = RunForOneOriginal((bool*)runForOneArray, _countof(runForOneArray));
	}
	test.Stop();
	this->mPerformanceTests.Add("RunForOneOriginal", FormatInt((SIZE_T)result - (SIZE_T)runForOneArray), test.GetElapsedTime());
	
	test.Start();
	for (int i = 0; i < 10000000; ++i)
	{
		result = RunForOneAlgo((bool*)runForOneArray, _countof(runForOneArray));
	}
	test.Stop();
	this->mPerformanceTests.Add("RunForOneAlgo", FormatInt((SIZE_T)result - (SIZE_T)runForOneArray), test.GetElapsedTime());
}

void CryPerformanceTest::VectorBoolOrBitsetTest()
{
	PerformanceTest test;
	Vector<bool> test_one;
	test_one.Reserve(100000);
	Bits test_two;
	test_two.Reserve(100000);
	std::bitset<100000> test_three;
	Bits test_six;
	test_six.Reserve(100000);
	Bits test_seven;
	test_seven.Reserve(100000);
	int result = 0;
	this->mPerformanceTests.Clear();
	
	// Vector with random values for the regular bool functions with no input assumptions.
	Vector<unsigned char> rand;
	rand.Reserve(100000);
	for(int i = 0; i < 100000; i++)
	{
		rand.Add((unsigned char)Random(100));
	}
	
	test.Start();
	for (int i = 0; i < 1000; ++i)
	{
		result = VectorBoolOrBitsetTestVectorBool(test_one, rand);
	}
	test.Stop();
	this->mPerformanceTests.Add("VectorBoolOrBitsetTestVectorBool", result, test.GetElapsedTime());
	
	test.Start();
	for (int i = 0; i < 1000; ++i)
	{
		result = VectorBoolOrBitsetTestBitSet(test_two, rand);
	}
	test.Stop();
	this->mPerformanceTests.Add("VectorBoolOrBitsetTestBitSet", result, test.GetElapsedTime());
	
	test.Start();
	for (int i = 0; i < 1000; ++i)
	{
		result = VectorBoolOrBitsetTestStdBitSet(test_three, rand);
	}
	test.Stop();
	this->mPerformanceTests.Add("VectorBoolOrBitsetTestStdBitSet", result, test.GetElapsedTime());
	
	test.Start();
	for (int i = 0; i < 1000; ++i)
	{
		result = VectorBoolOrBitsetTestBitSetVectorized(test_six, rand);
	}
	test.Stop();
	this->mPerformanceTests.Add("VectorBoolOrBitsetTestBitSetVectorized", result, test.GetElapsedTime());
	
	test.Start();
	for (int i = 0; i < 1000; ++i)
	{
		result = VectorBoolOrBitsetTestBitSetVectorizedAVX2(test_seven, rand);
	}
	test.Stop();
	this->mPerformanceTests.Add("VectorBoolOrBitsetTestBitSetVectorizedAVX2", result, test.GetElapsedTime());
	
	// Test whether Bits internal buffers are equal!
	int rawTwo;
	int rawSix;
	int rawSeven;
	const dword* raw_two = test_two.Raw(rawTwo);
	const dword* raw_six = test_six.Raw(rawSix);
	const dword* raw_seven = test_seven.Raw(rawSeven);
	
	// Check if the tests succeeded.
	String msgBox;
	if (rawTwo == rawSix && memcmp(raw_two, raw_six, rawTwo) == 0)
	{
		PromptOK("Vectorized test is correct! Buffers are equal.");
	}
	else
	{
		PromptOK("Vectorized test is incorrect! Buffers are not equal.");
	}
	
	if (rawTwo == rawSeven && memcmp(raw_two, raw_seven, rawTwo) == 0)
	{
		PromptOK("AVX2 vectorized test is correct! Buffers are equal.");
	}
	else
	{
		PromptOK("AVX2 vectorized test is incorrect! Buffers are not equal.");
	}
}

// ---------------------------------------------------------------------------------------------

GUI_APP_MAIN
{
	CryPerformanceTest* mainFrm = new CryPerformanceTest();
	mainFrm->Run();
	delete mainFrm;
}