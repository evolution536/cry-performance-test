#ifndef _CryPerformanceTest_ImlProvider_h_
#define _CryPerformanceTest_ImlProvider_h_

// IML file loader for pictures inside the program.
#define IMAGECLASS CryPerformanceTestIml
#define IMAGEFILE "CryPerformanceTest.iml"
#include <Draw/iml_header.h>

#endif