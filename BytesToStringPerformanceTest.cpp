#include "CryTestFunctions.h"

// ---------------------------------------------------------------------------------------------

// This is the original function.
String BytesToStringOriginal(const Byte* const buffer, const unsigned int length)
{
	String retVal;
	
	for (unsigned int i = 0; i < length; i++)
	{
		// Skip the space if the last byte is reached.
		if (i + 1 == length)
		{
			retVal += Format("%02X", *(buffer + i), 2);
		}
		else
		{
			retVal += Format("%02X ", *(buffer + i), 2);
		}
	}
	
	return retVal;
}

// This is the second fastest function around. (3 times faster than the original).
String BytesToStringEvoSolution(const Byte* const buffer, const unsigned int length)
{
	char* retVal = new char[length * 3 + 1];
	const char* const delPtr = retVal;
	const Byte* iterator = buffer;
	const Byte* const endIterator = iterator + length;
	
	while (iterator < endIterator)
	{
		sprintf(retVal, "%02X ", *iterator++);
		retVal += 3;
	}
	
	if (iterator >= endIterator)
	{
		*(retVal - 1) = 0;
	}
	
	String outString = delPtr;
	delete[] delPtr;
	return outString;
}

const char ascstr[] = "0123456789ABCDEF";

// This is the fastest function around. (103 times faster than the original).
String BytesToStringCustomizedLearnMore(const Byte* const buffer, const unsigned int length)
{
	char* retVal = new char[length * 3 + 1];
	const char* const delPtr = retVal;
	const Byte* iterator = buffer;
	const Byte* const endIterator = iterator + length;
	
	while (iterator < endIterator)
	{
		*retVal++ = ascstr[(*iterator >> 4) & 0xF];
		*retVal++ = ascstr[*iterator++ & 0xF];
		*retVal++ = 0x20;
	}
	
	*(retVal - 1) = 0;
	
	String outString = delPtr;
	delete[] delPtr;
	return outString;
}

// This is the previous function but with a string buffer instead double allocation. (200 times faster than the original).
String BytesToStringCustomizedLearnMoreWithStringBuffer(const Byte* const buffer, const unsigned int length)
{
	StringBuffer outString(length * 3 + 1);
	char* outIterator = outString.Begin();
	const Byte* iterator = buffer;
	const Byte* const endIterator = iterator + length;
	
	while (iterator < endIterator)
	{
		*outIterator++ = ascstr[(*iterator >> 4) & 0xF];
		*outIterator++ = ascstr[*iterator++ & 0xF];
		*outIterator++ = 0x20;
	}

	*(outIterator - 1) = 0;
	
	return outString;
}

// ---------------------------------------------------------------------------------------------

unsigned long get_bytes(unsigned long ident) {
	static unsigned long token_list[0x100] = { 0 };
	if (!token_list[ident & 0xFF]) {
		token_list[ident & 0xFF] = (ascstr[(ident >> 4) & 0xF] << 0) +
			(ascstr[ident & 0xF] << 8) + 0x200000;
	}
	return token_list[ident & 0xFF];
}

String BytesToStringSd333221(const BYTE* const buffer, const unsigned int length) {
	StringBuffer outString(3 * length + 1 + sizeof(unsigned long));
	char* output = outString.Begin();

	for (unsigned int i = 0; i < length; ++i){
		*reinterpret_cast<unsigned long*>(output + 3 * i) = get_bytes(*(buffer + i));
	}
	
	*(outString.End() - sizeof(unsigned long)) = 0;
	
	outString.Strlen();
	return outString;
}

// ---------------------------------------------------------------------------------------------

// Utility union
union VectorCharsAscii
{
	__m128i vector;
	char ascstr[16];
};

// Another function based on the most optimized one, but with SSE intrinsics for some parts.
String BytesToStringCustomizedLearnMoreWithStringBufferSSE(const Byte* const buffer, const unsigned int length)
{
	StringBuffer outString(length * 3 + 1);
	char* outIterator = outString.Begin();
	
	// We prepare a vector register with the 16 characters in ascstr. This eliminates memory
	// accesses to the ascstr buffer. ascstr must be aligned to a 16-byte boundary!
	//const __m128i ascBase = _mm_load_si128((__m128i*)ascstr);
	
	VectorCharsAscii ascStruct;
	ascStruct.vector = _mm_load_si128((__m128i*)ascstr);
	
	unsigned int j = 0;
	for (unsigned int i = 0; i < length; ++i)
	{
		/*__m128i ascVec = _mm_shuffle_epi8(ascBase, _mm_set1_epi8((buffer[i] >> 4) & 0xF));
		outIterator[j++] = _mm_extract_epi8(ascVec, 0);
		ascVec = _mm_shuffle_epi8(ascBase, _mm_set1_epi8(buffer[i] & 0xF));
		outIterator[j++] = _mm_extract_epi8(ascVec, 0);*/
		outIterator[j++] = ascStruct.ascstr[(buffer[i] >> 4) & 0xF];
		outIterator[j++] = ascStruct.ascstr[buffer[i] & 0xF];
		outIterator[j++] = 0x20;
	}

	// Place the string terminator.
	outIterator[j] = 0;
	
	return outString;
}