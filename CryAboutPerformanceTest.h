#ifndef _CryPerformanceTest_CryAboutPerformanceTest_h_
#define _CryPerformanceTest_CryAboutPerformanceTest_h_

#include <CtrlLib/CtrlLib.h>

using namespace Upp;

// Represents the about window of the testing application.
class CryAboutPerformanceTest : public TopWindow
{
private:
	Label mDescription;
	Button mClose;

	virtual bool Key(dword key, int count)
	{
		if (key == K_ESCAPE)
		{
			this->Close();
			return true;
		}
		
		return false;
	};
	
	void CloseWindow();
	
	typedef CryAboutPerformanceTest CLASSNAME;
public:
	CryAboutPerformanceTest();
	~CryAboutPerformanceTest();
};

#endif