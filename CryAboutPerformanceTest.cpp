#include "CryAboutPerformanceTest.h"
#include "ImlProvider.h"

CryAboutPerformanceTest::CryAboutPerformanceTest()
{
	this->Title("About Cry Performance Analysis").Icon(CryPerformanceTestIml::AboutButton()).SetRect(0, 0, 300, 150);
	
	this->mClose <<= THISBACK(CloseWindow);
	
	*this
		<< this->mDescription.SetLabel("Generic performance testing application\r\n\r\nby evolution536").HSizePos(5, 5).TopPos(5, 120)
		<< this->mClose.SetLabel("OK").BottomPos(5, 25).RightPos(5, 60)
	;
}

CryAboutPerformanceTest::~CryAboutPerformanceTest()
{
	
}

void CryAboutPerformanceTest::CloseWindow()
{
	this->Close();
}