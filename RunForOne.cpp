#include "CryTestFunctions.h"

// Walks a boolean buffer until it finds a 1 (true). Original function.
const bool* RunForOneOriginal(const bool* AddressBuffer, const int maxCount)
{
	const bool* iterator = AddressBuffer;
	const bool* const endIterator = iterator + maxCount;
	while (!*iterator && iterator < endIterator)
	{
		++iterator;
	}
	
	return iterator;
}

// Now we try it with a builtin searching algorithm, see if it is any faster.
const bool* RunForOneAlgo(const bool* AddressBuffer, const int maxCount)
{
	for (; AddressBuffer != AddressBuffer + maxCount; ++AddressBuffer) {
        if (*AddressBuffer) {
            return AddressBuffer;
        }
    }
    return AddressBuffer + maxCount;
}