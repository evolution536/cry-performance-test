#include "CryTestFunctions.h"

// Original compare function using memcmp.
const bool EvoCompareAobOriginal(const ArrayOfBytes& input, const ArrayOfBytes& expected)
{
	return memcmp(input.Data, expected.Data, input.Size) == 0;
}

// Optimized function using an algorithm that only compares the first and last bytes until all matched.
const bool EvoCompareAobOptimized(const ArrayOfBytes& input, const ArrayOfBytes& expected)
{
	const Byte* first = input.Data;
	const Byte* end = input.Data + input.Size;
	const Byte* expectedFirst = expected.Data;
	const Byte* expectedEnd = expected.Data + expected.Size;
	
	while (first != end)
	{
		if (*first++ != *expectedFirst++ || *end-- != *expectedEnd--)
		{
			return false;
		}
	}
	
	return true;
}