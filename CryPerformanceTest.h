#ifndef _CryPerformanceTest_CryPerformanceTest_h_
#define _CryPerformanceTest_CryPerformanceTest_h_

#include <CtrlLib/CtrlLib.h>

using namespace Upp;

// Represents a performance test.
struct TestCase : public Moveable<TestCase>
{
	String TestName;
	Callback TestFunction;
};

// The performance test application main window.
class CryPerformanceTest : public TopWindow
{
private:
	MenuBar mMenuStrip;
	ArrayCtrl mPerformanceTests;
	
	void MainMenu(Bar& pBar);
	void FileMenu(Bar& pBar);
	void EditMenu(Bar& pBar);
	void HelpMenu(Bar& pBar);
	void RunTestsMenu(Bar& pBar);
	
	void ExitApplication();
	void ApplicationSettings();
	void AboutApplication();
	
	// Testing functions
	void BytesStringRunTest();
	void StringBytesRunTest();
	void Ipv4ToStringRunTest();
	void EvoStringStreamRunTest();
	void EvoAOBComparisonTest();
	void RunForOneTest();
	void VectorBoolOrBitsetTest();
	
	typedef CryPerformanceTest CLASSNAME;
public:
	CryPerformanceTest();
	~CryPerformanceTest();
};

#endif