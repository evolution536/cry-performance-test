#ifndef _CryPerformanceTest_PerformanceTestManager_h_
#define _CryPerformanceTest_PerformanceTestManager_h_

#include <Core/Core.h>

using namespace Upp;

#include "PerformanceTest.h"

// Represents the application manager that manages all performance tests.
class PerformanceTestManager
{
private:
	// Singleton code: private constructor, destructor and copy constructors.
	PerformanceTestManager();
	~PerformanceTestManager();
	
	PerformanceTestManager(PerformanceTestManager const&);
	void operator=(PerformanceTestManager const&);
public:
	static PerformanceTestManager* GetInstance()
	{
		static PerformanceTestManager instance;
		return &instance;
	}
};

#endif