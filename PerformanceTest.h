#ifndef _CryPerformanceTest_PerformanceTest_h_
#define _CryPerformanceTest_PerformanceTest_h_

#include <Core/Core.h>

using namespace Upp;

// Represents a performance test that executes code and measures its execution time.
class PerformanceTest
{
private:
	LARGE_INTEGER frequency;
	LARGE_INTEGER t1;
	LARGE_INTEGER t2;
public:
	PerformanceTest();
	~PerformanceTest();
	
	void Start();
	void Stop();
	
	const double GetElapsedTime() const;
};

#endif