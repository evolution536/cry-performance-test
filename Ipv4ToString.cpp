#include "CryTestFunctions.h"

// ---------------------------------------------------------------------------------------------

// Original stringstream based IPv4 address to string conversion function.
std::string Ipv4ToStringOriginal(const IPV4ADDRESS ipv4)
{
	unsigned char bytes[4];
	bytes[3] = ipv4 & 0xFF;
	bytes[2] = (ipv4 >> 8) & 0xFF;
	bytes[1] = (ipv4 >> 16) & 0xFF;
	bytes[0] = (ipv4 >> 24) & 0xFF;
	std::stringstream outstream;
	outstream << (int)bytes[0] << "." << (int)bytes[1] << "." << (int)bytes[2] << "." << (int)bytes[3];
	return outstream.str();
}

// Original function based on generic string concatenation, instead of stringstream.
std::string Ipv4ToStringOptimizedNoStringStream(const IPV4ADDRESS ipv4)
{
	std::string outval;
	outval.reserve(16);
	outval += (IntStr((ipv4 >> 24) & 0xFF) + ".").Begin();
	outval += (IntStr((ipv4 >> 16) & 0xFF) + ".").Begin();
	outval += (IntStr((ipv4 >> 8) & 0xFF) + ".").Begin();
	outval += IntStr(ipv4 & 0xFF).Begin();
	return outval;
}

// Yet, another good itoa implementation
// returns: the length of the number string
const int itoa_custom(const int value, char* sp, const int radix)
{
    char tmp[16];// be careful with the length of the buffer
    char *tp = tmp;
    int i;
    unsigned v;

    int sign = (radix == 10 && value < 0);    
    if (sign)
        v = -value;
    else
        v = (unsigned)value;

    while (v || tp == tmp)
    {
        i = v % radix;
        v /= radix; // v/=radix uses less CPU clocks than v=v/radix does
        if (i < 10)
          *tp++ = i+'0';
        else
          *tp++ = i + 'a' - 10;
    }

    int len = tp - tmp;

    if (sign) 
    {
        *sp++ = '-';
        len++;
    }

    while (tp > tmp)
        *sp++ = *--tp;

    return len;
}

// Optimized function using custom itoa function and only C-based operations.
std::string Ipv4ToStringOptimizedEvolution536(const IPV4ADDRESS ipv4)
{
	char ipbuffer[16];
	int strlength = itoa_custom((ipv4 >> 24) & 0xFF, ipbuffer, 10);
	ipbuffer[strlength++] = 0x2E;
	strlength += itoa_custom((ipv4 >> 16) & 0xFF, ipbuffer + strlength, 10);
	ipbuffer[strlength++] = 0x2E;
	strlength += itoa_custom((ipv4 >> 8) & 0xFF, ipbuffer + strlength, 10);
	ipbuffer[strlength++] = 0x2E;
	strlength += itoa_custom(ipv4 & 0xFF, ipbuffer + strlength, 10);
	ipbuffer[strlength] = 0;
	return std::string(ipbuffer);
}

static const unsigned char str_octets_len[] = {
    // Single digits
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    // Double digits
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    // Triple digits
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    3, 3, 3, 3, 3, 3
};

static char const * const str_octets[] = {
      "0",   "1",   "2",   "3",   "4",   "5",   "6",   "7",   "8",   "9",
     "10",  "11",  "12",  "13",  "14",  "15",  "16",  "17",  "18",  "19",
     "20",  "21",  "22",  "23",  "24",  "25",  "26",  "27",  "28",  "29",
     "30",  "31",  "32",  "33",  "34",  "35",  "36",  "37",  "38",  "39",
     "40",  "41",  "42",  "43",  "44",  "45",  "46",  "47",  "48",  "49",
     "50",  "51",  "52",  "53",  "54",  "55",  "56",  "57",  "58",  "59",
     "60",  "61",  "62",  "63",  "64",  "65",  "66",  "67",  "68",  "69",
     "70",  "71",  "72",  "73",  "74",  "75",  "76",  "77",  "78",  "79",
     "80",  "81",  "82",  "83",  "84",  "85",  "86",  "87",  "88",  "89",
     "90",  "91",  "92",  "93",  "94",  "95",  "96",  "97",  "98",  "99",
    "100", "101", "102", "103", "104", "105", "106", "107", "108", "109",
    "110", "111", "112", "113", "114", "115", "116", "117", "118", "119",
    "120", "121", "122", "123", "124", "125", "126", "127", "128", "129",
    "130", "131", "132", "133", "134", "135", "136", "137", "138", "139",
    "140", "141", "142", "143", "144", "145", "146", "147", "148", "149",
    "150", "151", "152", "153", "154", "155", "156", "157", "158", "159",
    "160", "161", "162", "163", "164", "165", "166", "167", "168", "169",
    "170", "171", "172", "173", "174", "175", "176", "177", "178", "179",
    "180", "181", "182", "183", "184", "185", "186", "187", "188", "189",
    "190", "191", "192", "193", "194", "195", "196", "197", "198", "199",
    "200", "201", "202", "203", "204", "205", "206", "207", "208", "209",
    "210", "211", "212", "213", "214", "215", "216", "217", "218", "219",
    "220", "221", "222", "223", "224", "225", "226", "227", "228", "229",
    "230", "231", "232", "233", "234", "235", "236", "237", "238", "239",
    "240", "241", "242", "243", "244", "245", "246", "247", "248", "249",
    "250", "251", "252", "253", "254", "255"
};

static_assert((sizeof(str_octets) / sizeof(str_octets[0])) == 256, "stroctets is the wrong size.");
static_assert((sizeof(str_octets_len) / sizeof(str_octets_len[0])) == 256, "stroctetlen is the wrong size.");

// Optimized by 'chhere'. 40% faster than my original function!
std::string Ipv4ToStringOptimizedTablez(const IPV4ADDRESS ipv4)
{
    unsigned int pos;
    char ipbuffer[16];
    unsigned char octet;

    octet = (unsigned char)(ipv4 >> 24);
    strcpy(ipbuffer, str_octets[octet]);
    pos = str_octets_len[octet];
    ipbuffer[pos++] = '.';
    octet = (unsigned char)(ipv4 >> 16);
    strcpy(ipbuffer+pos, str_octets[octet]);
    pos += str_octets_len[octet];
    ipbuffer[pos++] = '.';
    octet = (unsigned char)(ipv4 >> 8);
    strcpy(ipbuffer+pos, str_octets[octet]);
    pos += str_octets_len[octet];
    ipbuffer[pos++] = '.';
    octet = (unsigned char)(ipv4);
    strcpy(ipbuffer+pos, str_octets[octet]);

    return std::string(ipbuffer);
}