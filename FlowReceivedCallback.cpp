#include "CryTestFunctions.h"

// ---------------------------------------------------------------------------------------------

const unsigned long SequenceCounter = 1337;
const IPV4ADDRESS SourceIpAddress = 320532705;
const WORD SourcePort = 7331;
const IPV4ADDRESS DestinationIpAddress = 3118805734;
const WORD DestinationPort = 1337;
const unsigned long BytesCount = 138570;
const unsigned long PacketCount = 24;

// Original function that creates an output string using std::stringstream.
std::string FlowReceivedCallback5Original()
{
	std::stringstream next;
	next << "FLOW " << SequenceCounter << ": " << Ipv4ToStringOptimizedEvolution536(SourceIpAddress) << ":" << SourcePort << " <> "
		<< Ipv4ToStringOptimizedEvolution536(DestinationIpAddress) << ":" << DestinationPort << " Bytes: " << BytesCount
		<< " Packets: " << PacketCount << " Protocol: " << "TCP";
	return next.str();
}

// Optimized function that uses a preallocated string rather than a stringstream.
std::string FlowReceivedCallback5NoStringStream()
{
	std::string next;
	next.reserve(512);
	next = "FLOW " + std::to_string(SequenceCounter) + ": " + Ipv4ToStringOptimizedEvolution536(SourceIpAddress) + ":" + std::to_string(SourcePort)
		+ " <> " + Ipv4ToStringOptimizedEvolution536(DestinationIpAddress) + ":" + std::to_string(DestinationPort) + " Bytes: " + std::to_string(BytesCount)
		+ " Packets: " + std::to_string(PacketCount) + " Protocol: " + "TCP";
	return next;
}

// Optmized function that uses plain itoa and a preallocated string.
std::string FlowReceivedCallback5ItoaOptimized()
{
	char buffer[512];
	int strLength = 5;
	memcpy(buffer, "FLOW ", 5);
	strLength += itoa_custom(SequenceCounter, buffer + strLength, 10);
	*(unsigned short*)(buffer + strLength) = 0x203A;
	strLength += sizeof(unsigned short);
	std::string nextIpRet = Ipv4ToStringOptimizedEvolution536(SourceIpAddress);
	memcpy(buffer + strLength, nextIpRet.c_str(), nextIpRet.length());
	strLength += nextIpRet.length();
	buffer[strLength++] = 0x3A;
	strLength += itoa_custom(SourcePort, buffer + strLength, 10);
	*(unsigned long*)(buffer + strLength) = 0x203E3C20;
	strLength += sizeof(unsigned long);
	nextIpRet = Ipv4ToStringOptimizedEvolution536(DestinationIpAddress);
	memcpy(buffer + strLength, nextIpRet.c_str(), nextIpRet.length());
	strLength += nextIpRet.length();
	buffer[strLength++] = 0x3A;
	strLength += itoa_custom(DestinationPort, buffer + strLength, 10);
	memcpy(buffer + strLength, " Bytes: ", 8);
	strLength += 8;
	strLength += itoa_custom(BytesCount, buffer + strLength, 10);
	memcpy(buffer + strLength, " Packets: ", 10);
	strLength += 10;
	strLength += itoa_custom(PacketCount, buffer + strLength, 10);
	memcpy(buffer + strLength, " Protocol: ", 11);
	strLength += 11;
	memcpy(buffer + strLength, "TCP", 3);
	strLength += 3;
	buffer[strLength] = 0;
	return std::string(buffer);
}