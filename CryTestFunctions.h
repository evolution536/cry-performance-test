#ifndef _CryPerformanceTest_CryTestFunctions_h_
#define _CryPerformanceTest_CryTestFunctions_h_

#include <Core/Core.h>
#include <bitset>

using namespace Upp;

// Utility functions for multiple testcases.
// ---------------------------------------------------------------------------------------------

const int itoa_custom(const int value, char* sp, const int radix);

// ---------------------------------------------------------------------------------------------

// Represents an array of bytes to be searched for in memory.
struct ArrayOfBytes
{
	Byte* Data;
	int Size;
	
	ArrayOfBytes()
	{
		this->Data = NULL;
		this->Size = 0;
	};

	// Assignment constructor, does not copy the data!
	ArrayOfBytes(Byte* const data, const int size)
	{
		this->Data = data;
		this->Size = size;
	};

	~ArrayOfBytes()
	{
		if (this->Data)
		{
			delete[] this->Data;
		}
		
		this->Data = NULL;
		this->Size = 0;
	};

	void Allocate(int size)
	{
		this->Data = new Byte[size];
		this->Size = size;
	};
	
	ArrayOfBytes(ArrayOfBytes const& next)
	{
		this->CopyConstructAob(next);
	};

	ArrayOfBytes& operator=(ArrayOfBytes const& next)
	{
		this->CopyConstructAob(next);
		return *this;
	};

private:
	inline void CopyConstructAob(ArrayOfBytes const& next)
	{
		this->Allocate(next.Size);
		memcpy(this->Data, next.Data, next.Size);
	};
};

// BytesToString functions
// ---------------------------------------------------------------------------------------------

__declspec(selectany) Byte constArray[] =
{
	0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0,
	0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0,
	0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0,
	0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0,
	0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0
};

String BytesToStringOriginal(const Byte* const buffer, const unsigned int length);
String BytesToStringEvoSolution(const Byte* const buffer, const unsigned int length);
String BytesToStringCustomizedLearnMore(const Byte* const buffer, const unsigned int length);
String BytesToStringCustomizedLearnMoreWithStringBuffer(const Byte* const buffer, const unsigned int length);
String BytesToStringSd333221(const BYTE* const buffer, const unsigned int length);
String BytesToStringCustomizedLearnMoreWithStringBufferSSE(const Byte* const buffer, const unsigned int length);

// StringToBytes functions
// ---------------------------------------------------------------------------------------------

__declspec(selectany) const char* testString = "00 00 40 AD 5D A3 4f 3C 00 a4 3D ff cc 00";

ArrayOfBytes StringToBytesOriginal(const String& input);
ArrayOfBytes StringToBytesOptimized(const String& input);
ArrayOfBytes StringToBytesLearnMoreOptimized(const String& input);

// Ipv4ToString functions
// ---------------------------------------------------------------------------------------------

typedef unsigned long IPV4ADDRESS;

const IPV4ADDRESS testIpv4 = 3282664774;

std::string Ipv4ToStringOriginal(const IPV4ADDRESS ipv4);
std::string Ipv4ToStringOptimizedNoStringStream(const IPV4ADDRESS ipv4);
std::string Ipv4ToStringOptimizedEvolution536(const IPV4ADDRESS ipv4);
std::string Ipv4ToStringOptimizedTablez(const IPV4ADDRESS ipv4);

// EvoStringStream functions
// ---------------------------------------------------------------------------------------------

std::string FlowReceivedCallback5Original();
std::string FlowReceivedCallback5NoStringStream();
std::string FlowReceivedCallback5ItoaOptimized();

// EvoAOBComparisonTest functions
// ---------------------------------------------------------------------------------------------

__declspec(selectany) Byte constCompareArray[] =
{
	0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x65, 0x70, 0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0,
	0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0,
	0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0,
	0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x75, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0,
	0x00, 0x10, 0x20, 0x25, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0, 0xB0, 0xCC, 0xD0, 0xE0, 0xF0
};

const bool EvoCompareAobOriginal(const ArrayOfBytes& input, const ArrayOfBytes& expected);
const bool EvoCompareAobOptimized(const ArrayOfBytes& input, const ArrayOfBytes& expected);

// EvoRunForOne functions
// ---------------------------------------------------------------------------------------------

__declspec(selectany) Byte runForOneArray[] =
{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0,
	0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0,
	0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0
};

const bool* RunForOneOriginal(const bool* AddressBuffer, const int maxCount);
const bool* RunForOneAlgo(const bool* AddressBuffer, const int maxCount);

// VectorBoolOrBitsetTest functions
// ---------------------------------------------------------------------------------------------

const int VectorBoolOrBitsetTestVectorBool(Vector<bool>& buffer, const Vector<unsigned char>& rand);
const int VectorBoolOrBitsetTestBitSet(Bits& buffer, const Vector<unsigned char>& rand);
const int VectorBoolOrBitsetTestStdBitSet(std::bitset<100000>& buffer, const Vector<unsigned char>& rand);
const int VectorBoolOrBitsetTestBitSetVectorized(Bits& buffer, const Vector<unsigned char>& rand);
const int VectorBoolOrBitsetTestBitSetVectorizedAVX2(Bits& buffer, const Vector<unsigned char>& rand);

#endif