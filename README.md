# Cry Performance test

This is my personal performance testing application. When I need to improve function/subroutine performance, I add code to this project. Feel free to add more tests to the application. The associated discussion is located at http://www.unknowncheats.me/forum/c-and-c/111406-discuss-optimizing-c-c-code.html

## Getting it to run on Windows

The project was created in Windows using TheIDE. This is the U++ framework IDE. The user interface is written using the rapid development framework U++. It can be found at http://ultimatepp.org/.

## Adding tests

* Add code file with your test functions. It is recommended that they have the same function prototype;
* Add function prototypes to the CryTestFunctions.h file;
* In CryPerformanceTest.cpp, add a test function to the user interface that executes all the subroutines/functions you added;

```
// Add the Ipv4ToString testcase.
TestCase& exampleTest = GlobalDefinedTests.Add();
exampleTest.TestName = "Example";
exampleTest.TestFunction = THISBACK(RunExampleTest);
```

The test function could contain the following code, for each subroutine that needs to be timed.

```
void CryPerformanceTest::RunExampleTest()
{
	PerformanceTest test;
	String result;
	this->mPerformanceTests.Clear();
	
	test.Start();
	for (int i = 0; i < 100000; ++i)
	{
		result = ExampleSubroutineOriginal(exampleParam);
	}
	test.Stop();
	this->mPerformanceTests.Add("ExampleSubroutineOriginal", result, test.GetElapsedTime());
}
```